echo "Euler"
echo "solution 1"
./0001.out 1000

echo "solution 2"
./0002.out 4000000

echo "solution 3"
./0003.out 600851475143

echo "solution 4"
./0004.out 100 999

echo "solution 5"
./0005.out 1 20

echo "solution 6"
./0006.out 100
