#include<iostream>
#include<stdlib.h>
#include<cmath>

using namespace std;

int main(int argc, char* argv[]) {
	long long sum_of_sqr = 0;
	long long sqr_of_sum = 0;
	int tot_num = 0;
	if (argc != 2) {
		cout << "Only one number argument" << endl;
		return 0;
	}
	tot_num = atoi(argv[1]);

	for (int i = 1; i <= tot_num; ++i ) {
		sum_of_sqr += (long long)pow(i,2);
		sqr_of_sum += i;
	}
	cout << (long long)pow(sqr_of_sum,2) - sum_of_sqr<< endl;
}
