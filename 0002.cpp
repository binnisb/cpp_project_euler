#include<iostream>
#include<stdlib.h>
using namespace std;


int main(int argc, char* args[]) {
	int max_number = 0;
	if (argc != 2) {
		cout << "Wrong arguments, just one number"<<endl;
		return 0;
	}
	max_number = atoi(args[1]);

	int low = 1;
	int high = 2;
	int next = 0;
	int tot = 2;
	while (high < max_number) {
		next = low + high;
		if (next % 2 == 0) {
			tot += next;
		}
		low = high;
		high = next;
	}
	cout << tot << endl;
	return 0;

}
