#include<iostream>
#include<stdlib.h>
#include<vector>
#include<unordered_set>
using namespace std;

int main(int argc, char* args[]) {
	int max_number = 0;
	unordered_set<int> multiply_3_or_5;
	if (argc != 2) {
		cout << "Wrong arguments, just one number"<<endl;
		return 0;
	}
	max_number = atoi(args[1]);
	for (int i = 1; 3*i < max_number; i++) {
		multiply_3_or_5.insert(3*i);
		if (5*i < max_number) {
			multiply_3_or_5.insert(5*i);
		}
	}
	int tot = 0;
	for ( auto num : multiply_3_or_5 ) {
		tot += num;
	}
	cout << tot << endl;
	return 0;

}
