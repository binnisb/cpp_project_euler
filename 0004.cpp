#include<iostream>
#include<stdlib.h>
#include<unordered_set>
#include<string>
#include<algorithm>
using namespace std;

bool isPalindrome(int pal) {
	string s1 = to_string(pal);
	string s2 = to_string(pal);
	reverse(s2.begin(),s2.end());
	if (s1 == s2) {
		return true;
	}
	else {
		return false;
	}
}

int main(int argc, char* args[]) {
	int low = 0;
	int high = 0;
	if (argc != 3) {
		cout << "Wrong arguments, just two numbers"<<endl;
		return 0;
	}
	low = atoi(args[1]);
	high = atoi(args[2]);
	unordered_set<int> possible_palindromes;
	for (low; low <= high; ++low) {
		for (int running = low; running <= high; ++running) {
			possible_palindromes.insert(low*running);
		}
	}
	int max_palindrome = 0;
	for (auto mul : possible_palindromes) {
		if ( isPalindrome(mul) ) {
			if (mul > max_palindrome) {
				max_palindrome = mul;
			}
		}
	}
	cout << max_palindrome << endl;
	return 0;

}
