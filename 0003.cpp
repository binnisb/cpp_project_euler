#include<iostream>
#include<stdlib.h>
#include<unordered_set>
#include<cmath>
using namespace std;

int main(int argc, char* args[]) {
	long long number = 0;
	if (argc != 2) {
		cout << "Wrong arguments, just one number"<<endl;
		return 0;
	}
	number = atoll(args[1]);
	//automatic floor of the sqrt
	long long max_possible = sqrt(number);
	unordered_set<long long> primes;
	for (long long i = 2; i <= max_possible; ++i) {
		if (number % i == 0){
			primes.insert(i);
			number = number / i;
			--i;
			continue;
		}
	}
	int max_prime = 0;
	for (auto i : primes) {
		if (i > max_prime) {
			max_prime =  i;
		}
	}
	cout << max_prime << endl;
	return 0;

}
