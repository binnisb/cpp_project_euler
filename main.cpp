#include <iostream>
#include <stdlib.h>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <cmath>
#include <string>
#include <algorithm>

using namespace std;

unordered_map<int,int> factor_to_primes(int num);
bool isPalindrome(int pal);

int e0001(int max_number);
int e0002(int max_number);
int e0003(long long number);
int e0004(int low, int high);
int e0005(int low, int high);
long long e0006(int tot_num);
int e0007(int how_many_primes);
int e0008(string str);
int e0009(int pythagorian_triplet);

int main(int argc, char* args[])
{
    int result = 0;
/*
    cout << "Sol 1" << endl;
    result = e0001(1000);
    cout << "Sol 2" << endl;
    result = e0002(4000000);
    cout << "Sol 3" << endl;
    result = e0003(600851475143);
    cout << "Sol 4" << endl;
    result = e0004(100, 999);
    cout << "Sol 5" << endl;
    result = e0005(1, 20);
    cout << "Sol 6" << endl;
    result = e0006(100);
    cout << "Sol 7" << endl;
    result = e0007(10001);
    cout << "Sol 8" << endl;
    result = e0008("73167176531330624919225119674426574742355349194934\
96983520312774506326239578318016984801869478851843\
85861560789112949495459501737958331952853208805511\
12540698747158523863050715693290963295227443043557\
66896648950445244523161731856403098711121722383113\
62229893423380308135336276614282806444486645238749\
30358907296290491560440772390713810515859307960866\
70172427121883998797908792274921901699720888093776\
65727333001053367881220235421809751254540594752243\
52584907711670556013604839586446706324415722155397\
53697817977846174064955149290862569321978468622482\
83972241375657056057490261407972968652414535100474\
82166370484403199890008895243450658541227588666881\
16427171479924442928230863465674813919123162824586\
17866458359124566529476545682848912883142607690042\
24219022671055626321111109370544217506941658960408\
07198403850962455444362981230987879927244284909188\
84580156166097919133875499200524063689912560717606\
05886116467109405077541002256983155200055935729725\
71636269561882670428252483600823257530420752963450");
*/
    cout << "Sol 9" << endl;
    result =  e0009(1000);

    return 0;
}


int e0001(int max_number) {
	unordered_set<int> multiply_3_or_5;
	for (int i = 1; 3*i < max_number; i++) {
		multiply_3_or_5.insert(3*i);
		if (5*i < max_number) {
			multiply_3_or_5.insert(5*i);
		}
	}
	int tot = 0;
	for ( auto num : multiply_3_or_5 ) {
		tot += num;
	}
	cout << tot << endl;
	return tot;

}

int e0002(int max_number) {
	int low = 1;
	int high = 2;
	int next = 0;
	int tot = 2;
	while (high < max_number) {
		next = low + high;
		if (next % 2 == 0) {
			tot += next;
		}
		low = high;
		high = next;
	}
	cout << tot << endl;
	return tot;
}

int e0003(long long number) {
	//automatic floor of the sqrt
	long long max_possible = sqrt(number);
	unordered_set<long long> primes;
	for (long long i = 2; i <= max_possible; ++i) {
		if (number % i == 0){
			primes.insert(i);
			number = number / i;
			--i;
			continue;
		}
	}
	int max_prime = 0;
	for (auto i : primes) {
		if (i > max_prime) {
			max_prime =  i;
		}
	}
	cout << max_prime << endl;
	return max_prime;

}

int e0004(int low, int high) {
	unordered_set<int> possible_palindromes;
	for (low; low <= high; ++low) {
		for (int running = low; running <= high; ++running) {
			possible_palindromes.insert(low*running);
		}
	}
	int max_palindrome = 0;
	for (auto mul : possible_palindromes) {
		if ( isPalindrome(mul) ) {
			if (mul > max_palindrome) {
				max_palindrome = mul;
			}
		}
	}
	cout << max_palindrome << endl;
	return max_palindrome;

}



int e0005(int low, int high) {
	unordered_map<int,int> tot;
	for (int i = low; i<= high; ++i) {
		unordered_map<int,int> v = factor_to_primes(i);
		for (auto& vs : v) {
			int& val = tot[vs.first];
			val = max(val,vs.second);
		}
	}
	int result = 1;
	for (auto i : tot) {
		result *= pow(i.first,i.second);
	}
	cout << result << endl;

	return result;
}

long long e0006(int tot_num) {
	long long sum_of_sqr = 0;
	long long sqr_of_sum = 0;

	for (int i = 1; i <= tot_num; ++i ) {
		sum_of_sqr += (long long)pow(i,2);
		sqr_of_sum += i;
	}
	long long diff = (long long)pow(sqr_of_sum,2) - sum_of_sqr;
	cout << diff << endl;
	return diff;
}

int e0007(int how_many_primes) {
    unordered_set<int> primes;
    primes.insert(2);
    int check_prime = 3;
    bool is_prime;
    while (primes.size() < how_many_primes) {
        is_prime = true;
        for (auto prime : primes) {
            if (check_prime % prime == 0) {
                is_prime = false;
                break;
            }
        }
        if (is_prime) {

            primes.insert(check_prime);
        }
        check_prime += 2;
    }
    check_prime -= 2;
    cout << check_prime << endl;
    return check_prime;
}

int e0008(string long_number) {
    int max_num = 0;
    int comp_num = 0;
    for (int i = 0; i < long_number.length() - 5; ++i) {
        comp_num =  atoi(long_number.substr(i,1).c_str()) * \
                    atoi(long_number.substr(i+1,1).c_str()) * \
                    atoi(long_number.substr(i+2,1).c_str()) * \
                    atoi(long_number.substr(i+3,1).c_str()) * \
                    atoi(long_number.substr(i+4,1).c_str());
        max_num = max(max_num,comp_num);
    }
    cout << max_num << endl;
    return max_num;

}

int e0009(int pytagorian_triplet) {
    for (int a = 1; a <= pytagorian_triplet / 2; ++a) {
        for (int b = a; b <= pytagorian_triplet / 2; ++b) {
            int c = pytagorian_triplet - a - b;
            if (pow(a,2) + pow(b,2) == pow(c,2) ) {
                    cout << a * b * c << endl;
            }
        }
    }
}

bool isPalindrome(int pal) {
	string s1 = to_string(pal);
	string s2 = to_string(pal);
	reverse(s2.begin(),s2.end());
	if (s1 == s2) {
		return true;
	}
	else {
		return false;
	}
}

unordered_map<int,int> factor_to_primes(int num) {
	int max_possible = sqrt(num);
	unordered_map<int,int> primes;
	for (int i = 2; i <= max_possible; ++i) {
		if (num % i == 0){
			int &val = primes[i];
			++val;
			num = num / i;
			--i;
			continue;
		}
	}
	int &val = primes[num];
	if (!val) {
		++val;
	}
	return primes;
}
