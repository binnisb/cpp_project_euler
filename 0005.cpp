#include<iostream>
#include<stdlib.h>
#include<unordered_map>
#include<algorithm>
#include<cmath>
using namespace std;

unordered_map<int,int> primes(int num) {
	int max_possible = sqrt(num);
	unordered_map<int,int> primes;
	for (int i = 2; i <= max_possible; ++i) {
		if (num % i == 0){
			int &val = primes[i];
			++val;
			num = num / i;
			--i;
			continue;
		}
	}
	int &val = primes[num];
	if (!val) {
		++val;	
	}	
	return primes;
}


int main(int argc, char* args[]) {
	if (argc != 3) {
		cout << "Wrong arguments, just two numbers"<<endl;
		return 0;
	}
	int low = atoi(args[1]);
	int high = atoi(args[2]);

	unordered_map<int,int> tot;
	for (int i = low; i<= high; ++i) {
		unordered_map<int,int> v = primes(i);
		for (auto& vs : v) {
			int& val = tot[vs.first];
			val = max(val,vs.second);
		}
	}
	int result = 1;
	for (auto i : tot) {
		result *= pow(i.first,i.second);
	}
	cout << result << endl;

	return 0;
}
